---
title: "Untitled"
output: html_document
---
 
 

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

 
# Packages
```{r}

library(tidyverse)
library(magrittr)
library(here)
library(rvest)
library(assertthat)
library(ggmap)
library(glue)
library(sf)
library(snapbox)
library(cancensus)
library(RColorBrewer)

source(here('R','utilities.R'))
source(here('R','nearest_store_graph.R'))
source(here('R','neigh_census_stores.R'))
source(here('R','voronoi.R'))
```
 
#Keys
```{r}

mapBoxToken <- keyringr::decrypt_gk_pw('token mapToken')
Sys.setenv(MAPBOX_ACCESS_TOKEN=mapBoxToken)

cancensus_api_key <- keyringr::decrypt_gk_pw('cancensus_api_key key_value')
options(cancensus.api_key = cancensus_api_key)
options(cancensus.cache_path = "/home/charles/Projects/Census2SfSp/Cache")

mypwd <- keyringr::decrypt_gk_pw("api google")
ggmap::register_google(key = mypwd)

```
 

---
---


# IO

 
 
## Census + Neighbourhoods
```{r}

shp_neigh <- get_neigh()  %>% st_transform(crs=4326)
shp_all_island <- get_census(dataset = 'CA16',
           regions = list(CD= 2466) , 
           geo_format = 'sf')

list_neigh <- c('Dollard-Des Ormeaux', 'Roxboro', "L'Île-Bizard\nPierrefonds")

shp_neigh_rox_ddo_pierrefonds <- shp_neigh %>% filter(name %in% list_neigh)

```
 

## Stores
```{r}

shp_all <- get_shp_stores(shp_filter=shp_neigh) %>% st_transform(crs=4326)
shp_all %>% nrow

#Clean the store chain names
shp_all %<>% mutate( store_chain =  str_replace_all(str_to_title(store_chain), "_", " ") )


shp_all_rox_ddo_pierrefonds_raw <- shp_all %>% st_filter(shp_neigh_rox_ddo_pierrefonds)

#Clean Adonis - this is a problem with the website - not a geocoding error - wtf adonis


shp_all_rox_ddo_pierrefonds_1 <- shp_all_rox_ddo_pierrefonds_raw %>%
  filter( store_name != 'Marché Adonis Dollard-des-Ormeaux')

shp_adonis <- data.frame(  store_name = 'Marché Adonis Dollard-des-Ormeaux',
                            store_address ='4601 Boulevard des Sources, Roxboro, QC H8Y 3C5' ,
                            store_chain='Adonis',
                           city='mtl',
                           category='specialized',  
                           lat=45.501894374177425, 
                           lon=-73.81426238246472) %>%
  st_as_sf(coords=c('lon','lat'),crs=4326)

shp_adonis_buffer <- shp_adonis %>% st_transform(crs=3857) %>% st_buffer(100) %>% st_transform(crs=4326)
shp_adonis_buffer_large <- shp_adonis %>% st_transform(crs=3857) %>% st_buffer(3250) %>% st_transform(crs=4326)

shp_all_rox_ddo_pierrefonds <- rbind(shp_all_rox_ddo_pierrefonds_1 ,shp_adonis ) %>% 
  st_filter(shp_adonis_buffer)

```

# Intersect 
```{r}

shp_neigh_rox_ddo_pierrefonds_inter <- st_intersection(shp_neigh_rox_ddo_pierrefonds,
                                                      SfSpHelpers::bbox_polygon(shp_adonis_buffer_large))  

shp_neigh_rox_ddo_pierrefonds_inter_bbox <- SfSpHelpers::bbox_polygon(shp_neigh_rox_ddo_pierrefonds_inter)

shp_neigh_rox_ddo_pierrefonds_inter$geometry %>% plot
``` 

---
---


# Adonis logo
```{r}
library(png)
library(grid)
img <- readPNG(here('figures', "adonis.png"))
g <- rasterGrob(img, interpolate=TRUE)

```
 
#Inset map of Montreal
```{r}

library(ggthemes)
pMtlInset <- ggplot() +
        geom_sf(data = shp_all_island,
                     fill = "#b2b2b2", color = "black", size = 0.3) +
        geom_sf(data = shp_neigh_rox_ddo_pierrefonds_inter_bbox, color = "red", alpha=0.75, size=0.5) +
  coord_sf(  expand = FALSE) + 
        theme_map() +
        theme(panel.background = element_rect(fill = NULL))
```


# Map
```{r}

lng_center <- (st_bbox(shp_neigh_rox_ddo_pierrefonds_inter)$xmax+ st_bbox(shp_neigh_rox_ddo_pierrefonds_inter)$xmin)/2
lat_center <- (st_bbox(shp_neigh_rox_ddo_pierrefonds_inter)$ymax+ st_bbox(shp_neigh_rox_ddo_pierrefonds_inter)$ymin)/2
basemap <- get_googlemap(center = c(lon = lng_center, lat = lat_center),
                    zoom = 14, scale = 1,
                    maptype ='terrain',
                    color = 'color')


  p <-  ggmap(basemap ) + 
  #
  #
  #  ggplot() + 
  #layer_mapbox( map_style = mapbox_light(), st_bbox(shp_neigh_rox_ddo_pierrefonds_inter)) +
  xlab("") + 
  ylab("") + 
  #
  #
  geom_sf(data= shp_neigh_rox_ddo_pierrefonds_inter, aes(fill=name), lwd=0.1, alpha=0.5 ,  inherit.aes = FALSE) +
  geom_sf(data=shp_all_rox_ddo_pierrefonds,aes(col=store_chain),  inherit.aes = FALSE) + 
  #
  #
  annotation_custom(g, xmin=-73.815+0.002, xmax=-73.805+0.002, ymin=45.498, ymax=45.508 ) + #logo
 annotation_custom(ggplotGrob(pMtlInset),  xmin=-73.79-0.015, xmax=-73.79+0.025-0.015, ymin=45.48-0.01, ymax=45.48+0.025-0.01 ) + 
  #
  #
    coord_sf(datum = NA) + 
    theme_minimal() + 
    theme(plot.subtitle = element_text(colour = 'darkgrey'),
          text = element_text( face = "plain", colour = "black"  ,family = "Roboto Condensed" ) , 
          strip.text = element_text(colour = 'black'),
          legend.position = 'bottom',
          legend.direction = 'vertical') + 
  scale_fill_discrete(name='') + 
  guides( colour =  F) + 
    #
    #
  ggtitle('On the fence') + 
    labs(subtitle = 'Adonis, Des Sources Boulevard')



ggsave(filename = here('figures','adonis_map.png'),
       p)


```