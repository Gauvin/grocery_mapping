---
title: "Untitled"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

 
# Packages
```{r}

library(tidyverse)
library(magrittr)
library(here)
library(rvest)
library(assertthat)
library(ggmap)
library(glue)
library(sf)
library(snapbox)

source(here('R','utilities.R'))

```
 
#Keys
```{r}

mapBoxToken <- keyringr::decrypt_gk_pw('token mapToken')
Sys.setenv(MAPBOX_ACCESS_TOKEN=mapBoxToken)


```


# Params
```{r}

city <- 'qc'

 
```

---
---


# GGplot theme
```{r}

custom_minimal_theme <- function(){  ggplot2::theme_minimal(base_family="Roboto Condensed", base_size=11.5) }

```


---
---


# IO


## Csv
```{r}

df_all <- read_csv( here('output', glue('df_all_{city}.csv'))) 
df_all %>% dim

```
## Neighbourhoods
```{r}

shp_neigh <- read_neigh(city)

```


# Data eng


## Conversion to shp + spatial filtering
```{r}

shp_all <- df_all %>% 
  st_as_sf(coords=c('lon','lat'),crs=4326) %>% 
  st_filter(shp_neigh)
 
shp_all %>% nrow

```


## Large vs small grocery 
```{r}
 
list_shp <- get_list_by_category(shp_all,check_exhaustive=T)
 

```

---
---

# Polygon heatmaps


```{r}

list_shp_heat_all <- map( 1:length(list_shp), 
     function(.x){
       shp_poly_heatmap <- SfSpHelpers::get_polygon_heatmap(list_shp[[.x]],bw = 0.01, gsize = 3000)
       shp_poly_heatmap$type <- names(list_shp)[[.x]]
       return(shp_poly_heatmap)
     }
)
 
 

shp_heatmap_all <- do.call(rbind,list_shp_heat_all)
 

```


---
---

# Plot 

```{r}

  
city_str <- ifelse(city=='qc','Quebe City','Montreal')

p <- ggplot() + 
  layer_mapbox( map_style = mapbox_dark(), st_bbox(shp_neigh)) + 
  geom_sf(data= shp_heatmap_all, aes(fill=colors), alpha=0.3, lwd=0 ) +
  #
  #
  facet_wrap(~type) + 
  #
  #
  coord_sf(datum = NA) + 
  custom_minimal_theme() + 
  theme(plot.background = element_rect(fill = "black"),
        text = element_text( face = "plain", colour = "white"  ), 
        strip.text = element_text(colour = 'white'),
        legend.position = 'none')+ 
  #
  #
  ggtitle('Grocery store density') + 
  labs(subtitle = glue('{city_str}') ,
       caption='Representative stores: \nLarge: IGA - Medium: Intermarché - Specialized: Rachel Berry - Ultra large: Costco') +
  #
  #
  scale_fill_viridis_d() 
    
    ggsave(here('figures',   glue('heatmap_large_vs_small_{city}.png')),
       p,
       width=10,
       height=7)

  
```
