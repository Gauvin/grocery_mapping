#' Connect to a table locally in postgres db with port 5433 listening and log in as postgres user
#'
#' @param db 
#'
#' @return
#' @export
#'
#' @examples
connectToDB <- function(db='montreal_neigh', port=5433 ){
  
  
  pwd <- keyringr::decrypt_gk_pw( glue("user postgres"))
  
  
  con <- DBI::dbConnect(RPostgres::Postgres(),
                        dbname = db,
                        host='localhost',
                        user='postgres',
                        password=pwd,
                        port=port
  )
  
  return(con)
}







#' Read the city neighbourhood from open data portal
#'
#' @param city 
#'
#' @return
#' @export
#'
#' @examples
read_neigh <- function(city){
  
  
  if(city=="qc"){
    
    con <- connectToDB('qcNeigh',port = 5432)
    queryStr <- glue( "SELECT * FROM public.\"vdq_quartier\";")
    
    shp_neigh <- sf::st_read(con,  query  = queryStr) %>% 
      sf::st_set_crs(4326)  
    
    #Make sure common name
    shp_neigh %<>% mutate(name=NOM_mod)
    shp_neigh %<>% mutate(borough=Arrondissement)
    
  }else{
    
    #Read custom 47 neighbourhoods
    con <- connectToDB('mtlNeigh',port = 5432)
    queryStr <- glue( "SELECT * FROM public.\"neighMixed47Rox\";")
    
    shp_neigh <- sf::st_read(con,  query  = queryStr) %>% 
      sf::st_set_crs(4326)  
    
    #Make sure common name
    shp_neigh %<>% mutate(name=nom_qr_mod)
    shp_neigh %<>% mutate(borough=nom_arr_mod)
    
  }
  
  return(shp_neigh )
}
